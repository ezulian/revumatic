import os
import re
import urllib3

from modules import utils

HTTPError = urllib3.exceptions.HTTPError

class CertError(Exception):
    pass


def https_pool(host, port=443, ca_certs=None, headers=None):
    """Opens a new urllib3 connection pool. If ca_certs is None, verifies
    the certificate using the system cert store. Raises CertError if the
    cert store is not found."""

    def find_ca_certs():
        if ca_certs is not None:
            paths = (ca_certs,)
        else:
            paths = ('/etc/pki/tls/certs/ca-bundle.crt',
                     '/etc/ssl/certs/ca-certificates.crt')
        for path in paths:
            if os.path.exists(path):
                return path
        raise CertError('No certificate store found')

    return urllib3.HTTPSConnectionPool(host, port, headers=headers,
                                       cert_reqs='CERT_REQUIRED', ca_certs=find_ca_certs())


def pool(host, port=None, ca_certs=None, headers=None):
    """Opens a new urllib3 connection pool, using either http or https. If
    'host' does not contain the protocol, http will be used."""
    if host.startswith('https://'):
        return https_pool(host[8:], port, ca_certs=ca_certs, headers=headers)
    host = utils.removeprefix(host, 'http://')
    return urllib3.HTTPConnectionPool(host, port, headers=headers)


def error_reason(exc):
    while hasattr(exc, 'reason'):
        exc = exc.reason
    return re.sub(r'^<[^>]*>: *', '', str(exc))
