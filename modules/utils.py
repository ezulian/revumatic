import os
import re
import subprocess

def oneline(s):
    return s.replace('\n', '↵')


def reflow(s):
    """Remove newlines and extra spaces from the string."""
    return re.sub(r'[\n ]+', ' ', s)


def capitalize(s):
    """Return a string with the first letter uppercased. Does not change the
    case of the rest of the string."""
    return s[0].upper() + s[1:]


def removeprefix(s, prefix):
    """Return a string with the given prefix removed. Does the same thing as
    s.removeprefix() but works on Python < 3.9."""
    if s.startswith(prefix):
        return s[len(prefix):]
    return s


def from_camel_case(s, sep='-'):
    """Convert from CamelCase to snake-case. Do not try to be too clever."""
    return s[0].lower() + re.sub(r'([A-Z]+)', sep + r'\1', s[1:]).lower()


def to_camel_case(s, sep='-'):
    """Covert from snake-case to CamelCase. Do not try to be too clever."""
    return s[0].upper() + re.sub(sep + '.', lambda m: m[0][1].upper(), s[1:])


def run(args, output_callback=None):
    """Runs the given program+args in a subprocess, feeding its output to
    the output_callback until the program finishes. Returns the program
    return code."""
    r, w = os.pipe()
    p = subprocess.Popen(args, stdout=w, stderr=subprocess.STDOUT)
    os.close(w)
    while True:
        buf = os.read(r, 1024).decode(errors='ignore')
        if output_callback:
            output_callback(buf)
        if p.poll() is not None:
            break
    os.close(r)
    return p.returncode


class Proxy:
    def __init__(self, name):
        self._proxy_name = name
        self._proxy_obj = None

    def __getattr__(self, attr):
        if self._proxy_obj is None:
            raise AttributeError('utils.{} was not initialized yet.'.format(self._proxy_obj))
        return getattr(self._proxy_obj, attr)


def init_lab(lab_instance):
    lab._proxy_obj = lab_instance

def is_lab_inited():
    return lab._proxy_obj is not None


# The global git.Lab instance.
lab = Proxy('lab')
