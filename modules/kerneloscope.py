import json
import weakref

from modules import connection

class KerneloscopeError(Exception):
    pass

class KerneloscopeResult(KerneloscopeError):
    pass

class Kerneloscope:
    def __init__(self, server, error_notify=None):
        self.error = None
        self.error_notify = None
        if error_notify:
            self.error_notify = weakref.WeakMethod(error_notify)
        self.pool = None
        if server:
            self.pool = connection.pool(server)

    def _call(self, name, **kwargs):
        if not self.pool:
            return None
        data = json.dumps(kwargs).encode('utf-8')
        try:
            resp = self.pool.request('POST', '/rpc/{}/'.format(name),
                                     body=data, headers={'Content-Type': 'application/json'},
                                     preload_content=False)
        except connection.HTTPError as e:
            raise KerneloscopeError(connection.error_reason(e))
        if resp.status != 200:
            raise KerneloscopeError('Server returned http error {}'.format(resp.status))

        data = json.load(resp)
        if 'error' in data:
            raise KerneloscopeResult('Server returned error {}'.format(data['error']))
        if 'result' not in data:
            raise KerneloscopeError('Unknown server response')
        return data['result']

    def call(self, name, **kwargs):
        try:
            return self._call(name, **kwargs)
        except KerneloscopeResult:
            return None
        except KerneloscopeError as e:
            self.error = str(e)
            notify = self.error_notify() if self.error_notify else None
            if notify:
                notify(self.error)
            return None

    def diff(self, commit):
        return self.call('get_diff', commit=commit)

    def fixes(self, upstream_list, repo_url=None, branch=None, tree=None):
        if tree is None:
            tree = ['try_url', '{}#{}'.format(repo_url, branch)]
        resp = self.call('get_missing_fixes', commit_with_subject=True, commits=upstream_list,
                         tree=tree)
        if not resp:
            return ()
        subjects = {}
        suggestions = {}
        for r in resp:
            subjects[r['commit']] = r['subject']
            suggestions[r['commit']] = not bool(r['kind'])
        result = []
        for r in resp:
            if r['added']:
                continue
            if 'fixes' in r and r['fixes']:
                result.extend((r['commit'], c, subjects[c], suggestions[c])
                              for c in r['fixes'])
        return result
