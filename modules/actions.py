import types
import tuimatic
import weakref

from modules.keys import basic_actions, display_keys
from modules.settings import settings


class BasicActions:
    """A replacement for tuimatic's CommandMap that picks keys from key_map.
    Allows only the basic actions."""
    def __init__(self):
        self._all_enabled = True
        self._update_action_map()
        tuimatic.connect_signal(settings, 'key-map-changed', self._update_action_map)

    def _update_action_map(self):
        self._action_map = {}
        for name in basic_actions:
            for key in settings.key_map[name]:
                self._action_map[key] = { 'name': name, 'enabled': False }

    def __getitem__(self, key):
        action = self._action_map.get(key, None)
        if action:
            return action['name']
        return None

    def set_all_actions_enabled(self, enabled):
        self._all_enabled = enabled
        self._update_action_map()

    def collect_help(self, data):
        return


class Actions(BasicActions, metaclass=tuimatic.MetaSignals):
    """A replacement for tuimatic's CommandMap that picks keys from key_map
    dynamically based on the configured and enabled actions."""
    signals = ['action']

    def __init__(self):
        self._actions = []
        self._actions_by_names = {}
        super().__init__()

    def _update_action_map(self):
        super()._update_action_map()
        if not self._all_enabled:
            return
        prios = {}
        for action in self._actions:
            if action['enabled']:
                for key in settings.key_map[action['name']]:
                    if action['priority'] >= prios.get(key, -1):
                        self._action_map[key] = action
                        prios[key] = action['priority']

    def add_actions(self, actions):
        """Adds the actions from an iterable. If an action with the same
        name is already present, the new one is ignored.
        Each action is a dictionary with these keys:
        name - name of the action; must be always present.
        prio - help priority, greater means more important; optional.
        help - help text; if not present, this action will not show in the
        bottom bar.
        long_help - help text for the help popup; if not present, the 'help'
        key will be used.
        enabled - initial state; optional, default is True.
        All extra keys will be sent as the second argument to the 'action'
        signal."""
        for data in actions:
            name = data['name']
            if self.has_action(name):
                continue
            extra = { key: data[key] for key in data
                      if key not in ('key', 'name', 'prio', 'help', 'long_help', 'enabled') }
            action = { 'name': name,
                       'help': data.get('help'),
                       'long_help': data.get('long_help') or data.get('help'),
                       'priority': data.get('prio', -1),
                       'extra': extra,
                       'enabled': data.get('enabled', True) }
            self._actions.append(action)
            self._actions_by_names[name] = action
        self._update_action_map()

    def has_action(self, action_name):
        return action_name in self._actions_by_names

    def set_action_enabled(self, action_name, enabled):
        if action_name not in self._actions_by_names:
            raise KeyError('{} not found in actions'.format(action_name))
        self._actions_by_names[action_name]['enabled'] = enabled
        self._update_action_map()

    def collect_help(self, data):
        if not self._all_enabled:
            return
        for action in self._actions:
            if not action['help'] and not action['long_help']:
                continue
            priority = action['priority']
            if priority not in data:
                data[priority] = []
            help_keys = [display_keys.get(key, key)
                         for key in settings.key_map[action['name']]]
            data[priority].append((action['enabled'], help_keys,
                                   action['help'], action['long_help']))

    def keypress(self, key, widget, size):
        if not self._all_enabled or key not in self._action_map:
            return key
        action = self._action_map[key]
        if not action['enabled']:
            return key
        args = [action['name'], widget, size]
        if action['extra']:
            args.append(action['extra'])
        tuimatic.emit_signal(self, 'action', *args)
        return None


def enable_actions(widget):
    """Modifies the widget to contain its own Actions instance and
    overrides its keypress method to call into _command_map.keypress. It's
    safe to call this repeatedly."""
    if isinstance(widget._command_map, Actions):
        return

    orig_keypress = widget.keypress

    def keypress(self, size, key):
        key = orig_keypress(size, key)
        if key:
            key = self._command_map.keypress(key, self, size)
        return key

    widget._command_map = Actions()
    widget.keypress = types.MethodType(keypress, widget)


class ActionTracker:
    def __init__(self, confirm_func, action_callback):
        self.widgets = weakref.WeakSet()
        self.callback = weakref.WeakMethod(action_callback)
        self.confirm_func = confirm_func

    def add_actions(self, widget, actions):
        enable_actions(widget)
        widget._command_map.add_actions(actions)
        if widget in self.widgets:
            return
        self.widgets.add(widget)
        tuimatic.connect_signal(widget._command_map, 'action', self._callback)

    def set_action_enabled(self, action_name, enabled, optional=True):
        count = 0
        for widget in self.widgets:
            try:
                widget._command_map.set_action_enabled(action_name, enabled)
                count += 1
            except KeyError:
                pass
        if not count and not optional:
            raise KeyError('{} not found in actions'.format(action_name))

    def _callback(self, name, widget, size, extra={}):
        callback = self.callback()
        if not callback:
            return
        confirm = extra.get('confirm')
        if confirm:
            self.confirm_func(name, confirm, callback, (name, widget, size))
        else:
            callback(name, widget, size)


def replace_command_map():
    cmd_map = BasicActions()
    tuimatic.command_map.command_map = cmd_map
    tuimatic.main_loop.command_map = cmd_map
    tuimatic.Widget._command_map = cmd_map

replace_command_map()
